#!/usr/bin/env python3

import os,json

from grafana_api.grafana_face import GrafanaFace

# https://github.com/m0nhawk/grafana_api
# GRAFANA_HOST
# GRAFANA_USER
# GRAFANA_PASSWORD

if __name__ == "__main__":
  g = GrafanaFace(host=os.environ.get('GRAFANA_HOST'),port=3000,auth=(os.environ.get('GRAFANA_USER'),os.environ.get('GRAFANA_PASSWORD')))
  dlist = g.search.search_dashboards()
  if 'TMPDIR' in os.environ:
    output_dir = os.environ.get('TMPDIR')
  else:
    output_dir = "/var/tmp/"

  for dash in g.search.search_dashboards():
    rawdash = g.dashboard.get_dashboard(dash['uid'])
    fname = output_dir+ '/' + dash['title']+".json"
    print (dash['uid'],fname)
    jdash = rawdash['dashboard']

    # Remove the UID, so we don't
    del jdash['uid']

    with open(fname,'w') as f:
      json.dump(jdash,f)




